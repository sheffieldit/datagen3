﻿using System;
using iVan.DAL;
using Microsoft.VisualBasic;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Principal;
using System.Diagnostics;
using System.Xml;
using System.IO;
using System.IO.Packaging;
using Parse;

namespace DataGen3
{
    public partial class Form1 : Form
    {
        string LiveConnString = "Data Source=morbo;Initial Catalog=ivan;Persist Security Info=True;User ID=sa;Password=pc1974;";
        string eml = "sarah.clarke@bestway.co.uk;bradley.leather@bestway.co.uk;daina.swann@bestway.co.uk;taylor.bowskill@bestway.co.uk";
        //string savePath = @"\\she-nt-fs1\IT\ivan\liveData";
        string savePath = @"C:\tmp\DataGenTest\liveData";
        bool ITUser = true;
        Dictionary<String, String> routes = new Dictionary<String, String>();
        List<string> errorRoutes = new List<string>();

        SashidoKeys sk = new SashidoKeys();
        public Form1()
        {
            InitializeComponent();
            this.Paint += new PaintEventHandler(set_background);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //this.TransparencyKey = Color.Black;

            bool instanceCountOne = false;
            using (Mutex mtex = new Mutex(true, this.Name, out instanceCountOne))
            {
                if (!instanceCountOne)
                {
                    MessageBox.Show("DataGen is already running!");
                    Application.Exit();
                }
            }

             if (Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName).Count() > 1)
            {
                MessageBox.Show("Only one instance can run at once!");
                Application.Exit();
            }
        }
        public async void FormLoad() {

            timer1.Enabled = false;
            lblProjTime.Text = "";
            string currUser = Environment.UserName.ToString().ToLower();
            List<string> UserGroups = GetGroups(currUser);
            DateTime lFileDateTime = File.GetLastWriteTime(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).Replace(@"file:\", "") + @"\DataGen3.exe");
            DateTime lDateBuild = new DateTime(lFileDateTime.Ticks);
            this.Text = $"Sashido Data Gen: {lDateBuild.ToLongDateString()} {lDateBuild.ToLongTimeString()}";
            if (!UserGroups.Contains("BESTWAYWHSALE\\DEPT_Sheffield_IT") || currUser == "Sheffield.Reports")
            {
                ITUser = false;
                HideButtons();
            }

            LoadRoutes();
            SellingDay.MinDate = DateTime.Today;
            SellingDay.MaxDate = DateTime.Today.AddDays(14);
            string nwd = "";
            nwd = await RunSQL("SELECT MIN(Date) FROM zeus.dbo.MasterPeriods2Wk WHERE Date > GETDATE() AND RSDay BETWEEN 2 AND 6");
            SellingDay.Value = Convert.ToDateTime(nwd);

            //gary testing setup
            SendToLive.Checked = false;
            //ShowErrors.Checked = false;
            PerformClean.Checked = false;
            UpdateGenerics.Checked = false;
            NotificationsChk.Checked = false;
            //lstViewRoutes.Items.Clear();
            //lstViewRoutes.Items.Add("220");
        }
        private void HideButtons() { }
        private void set_background(Object sender, PaintEventArgs e)
        {
            Graphics graphics = e.Graphics;

            //the rectangle, the same size as our Form
            Rectangle gradient_rectangle = new Rectangle(0, 0, Width, Height);

            //define gradient's properties
            Brush b = new LinearGradientBrush(
                    gradient_rectangle,
                    Color.FromArgb(12, 179, 65),
                    Color.FromArgb(57, 128, 227),
                    65f);

            //apply gradient         
            graphics.FillRectangle(b, gradient_rectangle);
        }
        public async Task<string> RunSQL(string sql, string db = "ivan", Int16 index = 0)
        {
            await Task.Delay(1);
            SqlConnection SQLCon = new SqlConnection
            {
                ConnectionString = LiveConnString
            };
            SQLCon.Open();
            SqlCommand sqlCmd = new SqlCommand(sql, SQLCon) { CommandTimeout = 6000 };
            SqlDataReader rdr = sqlCmd.ExecuteReader();
            string returnValue = "";

            while (rdr.Read())
            {
                returnValue = rdr[index].ToString();
            }
            rdr.Close();
            SQLCon.Close();

            return returnValue;
        }
        public async Task SQLUpdate(string sql, string db = "ivan", Boolean retry = false, Boolean logIt = false)
        {
            await Task.Delay(1);
            SqlConnection SQLCon = new SqlConnection
            {
                ConnectionString = LiveConnString
            };
            SQLCon.Open();
            SqlCommand sqlCmd = new SqlCommand(sql, SQLCon) { CommandTimeout = 1800 };
            Int16 errCnt = 0;
            string errMsg = "";
            Int16 totCnt = 0;

            tryAgain:
            errMsg = "";
            try
            {
                sqlCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                Thread.Sleep(10000);
                errCnt += 1;
                if (errCnt == 5)
                {
                    goto endbit;
                }
                goto tryAgain;
            }
            SQLCon.Close();
            endbit:
            if (errCnt == 5)
            {
                //look, we have tried 5 times and it just wont work!
            }
            else
            {
                if (errCnt > 0 && retry)
                {
                    Thread.Sleep(30000);
                    totCnt += 1;
                    if (totCnt == 5)
                    {
                        //look, it's REALLY not working
                        MessageBox.Show("derp");
                    }
                    else
                    {
                        goto tryAgain;
                    }
                }
            }
        }
        private List<string> GetGroups(string userName)
        {
            List<string> result = new List<string>();
            WindowsIdentity wi = new WindowsIdentity(userName);

            foreach (IdentityReference group in wi.Groups)
            {
                try
                {
                    result.Add(group.Translate(typeof(NTAccount)).ToString());
                }
                catch (Exception)
                {
                }
            }
            result.Sort();
            return result;
        }
        private void LoadRoutes()
        {
            lstViewRoutes.Items.Clear();
            string sql = "select routeno,business from liveRoutes where retired = 0 AND liveDate <= DATEADD(dd,25,GETDATE()) order by business,routeno";
            if (!ITUser)
            {
                sql = "select routeNo,business from vwTMERoutes";
            }

            SqlConnection SQLCon = new SqlConnection
            {
                ConnectionString = LiveConnString
            };
            SQLCon.Open();
            SqlCommand cmd = new SqlCommand(sql, SQLCon) { CommandTimeout = 6000 };
            SqlDataReader rdr = cmd.ExecuteReader();
            
            while (rdr.Read())
            {
                lstViewRoutes.Items.Add(rdr["routeNo"].ToString());
                routes.Add(rdr["routeNo"].ToString(), rdr["business"].ToString());
            }
            rdr.Close();

            Business.Items.Clear();
            cmd.CommandText = "SELECT DISTINCT Business FROM LiveRoutes r WHERE liveDate < GETDATE()+25 AND retired=0 ORDER BY 1";
           
            rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                Business.Items.Add(rdr["Business"].ToString());
            }
            rdr.Close();

            ASM.Items.Clear();
            cmd.CommandText = "SELECT DISTINCT [Area Manager] ASM FROM speVSRInfoData ORDER BY 1";
            rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                ASM.Items.Add(rdr["ASM"].ToString());
            }

            rdr.Close();
            SQLCon.Close();

        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            FormLoad();
        }
        private async Task GenData()
        {
            string ans = "";
            string[] errArray = new string[]
            {
                "SELECT count(*) cnt FROM vwNBL_or_AGAP_OfferErrors|NBL or Availability Gap Errors - assigned to offer lists - REMOVE THEM NOW - YOU CANNOT CONTINUE - check view ivan.dbo.vwNBL_or_AGAP_OfferErrors !!",
                "SELECT count(*) cnt FROM vwNBLRangeOfferError|Invalid NBL Offer assigned to NBL Range Error - YOU CANNOT CONTINUE UNTIL THIS IS RESOLVED - check view ivan.dbo.vwNBLRangeOfferError !!",
                "SELECT count(*) cnt FROM vwNBLRangeAGOfferError|Invalid Availability Gap Offer assigned to NBL Range Error - YOU CANNOT CONTINUE UNTIL THIS IS RESOLVED - check view ivan.dbo.vwNBLRangeAGOfferError !!",
                "SELECT top 1 id from vwDupeNBLRanks|EXEC spFixDupeNBLRanks @@@",
                "SELECT COUNT(*) cnt FROM speVsrInfoData v WHERE RouteNo NOT IN (SELECT RouteNo FROM tblNBLRangeHeader h INNER JOIN tblNBLRoutes r ON h.id = r.tId) AND Region <> 'CAM' AND v.RouteNo NOT IN (SELECT DISTINCT SCRoute FROM dd_Depot WHERE SCRoute IS NOT NULL)|There are route(s) who have no NBL Range - this MUST be resolved before data-gen can proceed",
                "SELECT COUNT(*) cnt FROM vwDodgySpecialPrices|AAAAAGGGGHHHHH - Someone has entered special prices against pricing group 0000 - get IT to check view ivan.dbo.vwDodgySpecialPrices - dataGen cannot continue until this is resolved"
            };

            Int16 loopCnt = 0;

            foreach (string x in errArray)
            {
                tryagain:
                string sql = x.Split('|')[0];
                string err = x.Split('|')[1];
                string sqlout;

                sqlout = await RunSQL(sql);
                if (sqlout == "")
                {
                    sqlout = "0";
                }
                if (err.Contains("@@@") && sqlout != "0")
                {
                    loopCnt += 1;
                    if (loopCnt==10)
                    {
                        MessageBox.Show("Error in DupeNBLRanks - tried to fix 10 times - check view vwDupeNBLRanks");
                        goto endbit;
                    }
                    await SQLUpdate(err.Replace("@@@", sqlout));
                    goto tryagain;
                }
                if (Convert.ToInt16(sqlout) != 0)
                {
                    MessageBox.Show(err);
                    goto endbit;
                }
            }

            if (SendToLive.Checked)
            {
                string endOfPeriod = await RunSQL("SELECT COUNT(*) FROM zeus.dbo.MasterPeriods2Wk WHERE Date = CAST(GETDATE() AS DATE) AND RSDay = 6 AND RIGHT(SDWeek,1) = '2'");
                if (endOfPeriod == "1")
                {
                    MessageBox.Show("Are you mad? Today is the last day of a period, thus any data-gen created now for use today will not have offers and targets applicable to today in it - press OK to continue");
                    ans = Interaction.InputBox("If you want to continue and accept that this is madness, please type \'HELL NO\' in the box below");
                    if ((ans.ToUpper() == "HELL NO"))
                    {
                        MessageBox.Show("OK - on your head be it!");
                    }
                    else
                    {
                        MessageBox.Show("Wise move!");
                        goto endbit;
                    }
                }
            }

            ans = MessageBox.Show($"Are you sure you want to generate data to {Interaction.IIf(SendToLive.Checked, "LIVE", "TEST")}?","", MessageBoxButtons.YesNo).ToString();
            if (ans == DialogResult.No.ToString())
            {
                goto endbit;
            }

            await CleanData();
            await GenerateAllGenerics();
            await CmdGo();

            if (NotificationsChk.Checked)
            {
                string EmailBody = String.Format("<html><body><p>DataGen completed on {0}</p></body></html>", DateTime.Now.ToString());
                string EmailSQLCommand = String.Format("INSERT INTO HTMLEmail SELECT '{0}','{1}','','DataGen Completed',GETDATE()", EmailBody, eml);
                await SQLUpdate(EmailSQLCommand);
            }

            endbit:
            await Task.Delay(1);
        }

        private async void cmdGenData_Click(object sender, EventArgs e)
        {
            SwitchControls(false);
            InitParse();
            await GenData();
            SwitchControls(true);
        }
        private async Task CleanData()
        {
            if (!PerformClean.Checked) {return;}

            lstStatus.Items.Clear();
            lstStatus.Items.Insert(0, "Cleaning Data");
            await SQLUpdate("exec spCleanData");
            lstStatus.Items.Insert(0, "Cleaned Data");
        }
        private async Task GenerateAllGenerics()
        {
            if (!UpdateGenerics.Checked) { return; }
            string biz = "";

            for (int i = 0; i < Business.Items.Count; i++)
            {
                biz = Convert.ToString(Business.Items[i]).ToLower();
                lstStatus.Items.Insert(0, $"Uploading Generic Data {biz}");
                await GenerateGenericData(biz);
                lstStatus.Items.Insert(0, $"Uploaded Generic Data {biz}");
            }

            lstStatus.Items.Insert(0, "Generics Uploads Complete!");
        }
        private async Task CmdGo()
        {
            errorRoutes.Clear();
            lblCount.Text = "0/" + lstViewRoutes.Items.Count.ToString();
            ProgressBar1.Maximum = lstViewRoutes.Items.Count;
            int processedCount = 0;
            lstStatus.Items.Clear();
            DateTime dateStart = DateTime.Now;

            foreach (ListViewItem v in lstViewRoutes.Items)
            {
                string route = v.Text;
                lstViewRoutes.EnsureVisible(lstViewRoutes.Items.IndexOf(v));
                string company = routes[route].ToString();
                lstViewRoutes.FindItemWithText(route, true, 0, false).ForeColor = Color.Green;
                DateTime dateStartRoute = DateTime.Now;
                await CreateXML(route, company);
                DateTime dateEndRoute = DateTime.Now;
                lstStatus.Items.Insert(0, $"Processed {route} : Took {GetTimeDifferenceInMinutes(dateStartRoute, dateEndRoute)}");
                processedCount += 1;
                int prog = (processedCount / lstViewRoutes.Items.Count * 100);
                lblCount.Text = processedCount.ToString() + "/" + lstViewRoutes.Items.Count.ToString();
                ProgressBar1.Value = processedCount;

                runningTimeLbl.Text = GetTimeDifferenceInMinutes(dateStart, DateTime.Now);
                double elapsed = (DateTime.Now - dateStart).TotalSeconds;
                double remtime = elapsed / processedCount * (lstViewRoutes.Items.Count - processedCount);
                lblProjTime.Text = "Projected Remaining Time : " + GetTimeDifferenceInMinutes(DateTime.Now, DateTime.Now.AddSeconds(remtime));
                Application.DoEvents();
            }
            DateTime dateEnded = DateTime.Now;

            MessageBox.Show($"Uploaded {lstViewRoutes.Items.Count.ToString()} Records in {GetTimeDifferenceInMinutes(dateStart, dateEnded)}");
            string message = "";
            if (message.Length > 0)
            {
                MessageBox.Show(message);
            }
        }
        private async Task GenerateGenericData(string biz)
        {
            await CreateXMLFromStoredProc(biz, biz + "_newDefaultSalesPlanner", "iVan_MakeDefaultSalesPlanner");
            await CreateXMLFromStoredProc(biz, biz + "_DefaultDepots", "iVan_MakeDefaultDepots");
            await CreateXMLFromStoredProc(biz, biz + "_defaultProductGroups", "iVan_MakeDefaultProductGroups");
            await CreateXMLFromStoredProc(biz, biz + "_defaultValidUsers", "iVan_MakeValidUserList");
            await CreateXMLFromStoredProc(biz, biz + "_defaultCAMDeliveryPeriod", "iVan_MakeCAMDeliveryPeriodXML");
            await CreateXMLFromStoredProc(biz, biz + "_defaultPeriods", "iVan_MakePeriodXML");
            await CreateXMLFromStoredProc(biz, biz + "_defaultHierarchy", "iVan_defaultHierarchy");
            await CreateXMLFromStoredProc(biz, biz + "_defaultCustomerSurveyQuestions", "iVan_MakeDefaultCustomerSurveyQuestions");
            await CreateXMLFromStoredProc(biz, biz + "_defaultNBL", "iVan_MakeDefaultNBL");
            await CreateXMLFromStoredProc(biz, biz + "_defaultLoyalty", "iVan_MakeDefaultLoyalty");
            await CreateXMLFromStoredProc(biz, biz + "_defaultSupplierCampaigns", "iVan_MakeDefaultSupplierCampaigns");
            await CreateXMLFromStoredProc(biz, biz + "_defaultVehicleRegistrations", "iVan_MakeDefaultVehicleRegistrations");
            await CreateXMLFromStoredProc(biz, biz + "_defaultManagerSpecial", "iVan_MakeDefaultManagerSpecial");
            await CreateXMLFromStoredProc(biz, biz + "_defaultAbsoluteUnit", "iVan_MakeDefaultAbsoluteUnit");
            await CreateXMLFromStoredProc(biz, biz + "_defaultVapeCheck", "iVan_MakeDefaultVapeCheck");
            await ZipAndUpload("",biz.ToLower(),true);
        }
        private async Task CreateXMLFromStoredProc(string biz,string xmlFileName,string storedproc, bool isGeneric = true)
        {
            // if isGeneric = false then biz = route!
            string paramName = "biz";
            if (!isGeneric)
            {
                paramName = "route";
            }

            xmlFileName += ".xml";
            Application.DoEvents();
            SqlConnection con = new SqlConnection(LiveConnString);
            SqlCommand cmd = new SqlCommand(storedproc, con)
            {
                CommandType = CommandType.StoredProcedure,
                CommandTimeout = 1200
            };
            con.Open();

            try
            {
                cmd.Parameters.AddWithValue(paramName, biz);
                cmd.Parameters.AddWithValue("nextWorkingDay", SellingDay.Value);

                SqlParameter p2 = cmd.Parameters.Add("xml", SqlDbType.Xml);
                p2.Direction = ParameterDirection.Output;

                cmd.ExecuteNonQuery();
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(p2.Value.ToString());

                string sFolder = $@"{savePath}\Generic\{biz}";
                if (!isGeneric)
                {
                    sFolder = $@"{savePath}\route_{biz}";
                }
                string sFilename = $@"{sFolder}\{xmlFileName}";

                if (!Directory.Exists(sFolder))
                {
                    Directory.CreateDirectory(sFolder);
                }
                if (File.Exists(sFilename))
                {
                    File.Delete(sFilename);
                }
                xml.Save(sFilename);
                con.Close();
            }
            catch (Exception ex)
            {
                errorRoutes.Add($"{biz}{"\n"} Stored Proc : {storedproc}{"\n"}{ex.Message.ToString()}");
                lstViewRoutes.FindItemWithText(biz).ForeColor = Color.Red;
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                if (ShowErrors.Checked)
                {
                    MessageBox.Show(ex.Message.ToString());
                }
            }

            await Task.Delay(1);
        }
        
        private async Task ZipAndUpload(string route,string biz,bool isGeneric)
        {

            return;
            string sFolder = $@"{savePath}\Generic\{biz}";
            string sFolderBackup = $@"{savePath}\Backup\{biz}";
            string zipname = $"{biz}.zip";

            if (!isGeneric)
            {
                sFolder = $@"{savePath}\route_{route}";
                sFolderBackup = $@"{savePath}\Backup\route_{route}";
                zipname = $"{biz}_{route}.zip";
            }

            if (!Directory.Exists(sFolderBackup))
            {
                Directory.CreateDirectory(sFolderBackup);
            }

            string backupfn = $@"{sFolderBackup}\{DateTime.Now.ToString("yyyyMMdd")}{zipname}";

            try
            {
                string fn = $@"{sFolder}\{zipname}";
                if (File.Exists($@"{sFolder}\{zipname}"))
                {
                    //string backupfn = $@"{sFolderBackup}\Generic\{DateTime.Now.ToString("yyyyMMdd")}{zipname}";
                    if (File.Exists(backupfn))
                    {
                        File.Delete(backupfn);
                    }
                    File.Copy(fn, backupfn);
                    File.Delete(fn);
                }

                ZipFiles(sFolder, zipname);
                if (!isGeneric)
                {
                    biz = route;
                }
                await UploadToParse(sFolder, zipname, biz);
            }
            catch (Exception ex)
            {
                if (ShowErrors.Checked)
                {
                    MessageBox.Show($@"Zip Name : {zipname}{"\n"}Path : {sFolder}\{zipname}{"\n"}Error : {ex.Message.ToString()}");
                }
                //throw;
            }
        }
        private void ZipFiles(string path, string zipname)
        {
            Package zip = ZipPackage.Open($@"{path}/{zipname}", FileMode.OpenOrCreate, FileAccess.ReadWrite);
            DirectoryInfo di = new DirectoryInfo(path);
            FileInfo[] diar1 = di.GetFiles();
            //FileInfo dra;

            foreach (FileInfo dra in diar1)
            {
                if (!dra.ToString().Equals(zipname))
                {
                    XmlDocument gg = new XmlDocument();
                    try
                    {
                        if (Strings.Right(dra.ToString(),3).ToLower() == "xml")
                        {
                            gg.Load($@"{path}/{dra.ToString()}");
                            AddToArchive(zip, $@"{path}/{dra.ToString()}");
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show($"Bad Generics XML {dra.ToString()}{"\n"}{ex.InnerException.ToString()}");
                        //throw;
                    }
                }
            }
            zip.Close();
        }
        private void AddToArchive(Package zip, string fileToAdd)
        {
            //Replace spaces with an underscore (_) 
            string uriFileName = fileToAdd.Replace(" ", "_");

            //'A Uri always starts with a forward slash "/" 
            string zipUri = string.Concat("/", Path.GetFileName(uriFileName));

            Uri partUri = new Uri(zipUri, UriKind.Relative);
            string contentType = System.Net.Mime.MediaTypeNames.Application.Zip;

            PackagePart pkgPart = zip.CreatePart(partUri, contentType, CompressionOption.Normal);
            Byte[] bites = File.ReadAllBytes(fileToAdd);

            pkgPart.GetStream().Write(bites, 0, bites.Length);
        }
        private async Task UploadToParse(string path, string zipname, string routeno)
        {
            await Task.Delay(1);

            FileInfo info = new FileInfo($@"{path}/{zipname}");
            double fileSize = (info.Length / 1024)/1024;

            if (fileSize <= 9.99)
            {
                await UploadFile(routeno, zipname, path);
            }
            else
            {
                MessageBox.Show($"{zipname} totals {fileSize.ToString()} MB - it is too large!");
            }

        }
        private async Task UploadFile(string routeno,string zipname, string path)
        {
            string ParseClass = "ZipFiles";

            var query = ParseObject.GetQuery(ParseClass).Limit(1000).WhereEqualTo("name", $"{routeno}ZipFile");
            IEnumerable<ParseObject> results = await query.FindAsync();
            foreach (ParseObject p in results)
            {
                if (p.ContainsKey("zipfile"))
                {
                    try
                    {
                        await DelCloudFile(p.Get<ParseFile>("zipfile").Name.ToString(), ParseClass, "zipfile", p.ObjectId.ToString());
                    }
                    catch (Exception)
                    {
                        //well, we tried!
                    }
                }
                await p.DeleteAsync();
            }

            ParseObject myObject = new ParseObject(ParseClass);
            string fn = $@"{path}\{zipname}";
            if (File.Exists(fn))
            {
                FileStream file = new FileStream(fn, FileMode.Open, FileAccess.Read);
                using (FileStream fs = new FileStream(fn, FileMode.Open, FileAccess.Read))
                {
                    byte[] byteArray = new byte[fs.Length];
                    int bytesRead = 0;
                    int bytesToRead = (int)fs.Length;

                    while (bytesToRead > 0)
                    {
                        int read = file.Read(byteArray, bytesRead, bytesToRead);
                        if (read == 0)
                            break;
                        bytesToRead -= read;
                        bytesRead += read;
                    }

                    ParseFile pf = new ParseFile(zipname, byteArray, "file/zip");
                    await pf.SaveAsync();
                    myObject.Add("zipfile", pf);
                }
            }
            myObject.Add("name", $"{routeno}ZipFile");

            await myObject.SaveAsync();
        }
        private async Task DelCloudFile(string FileName, string xClass,string Field,string objectId)
        {
            Dictionary<string, object> iDic = new Dictionary<string, object>()
            {
                {"FileName",FileName},
                {"Class",xClass},
                {"Field",Field},
                {"objectId",objectId}
            };

            string ParseInstance = "iVanLive";

            if (!SendToLive.Checked)
            {
                await ParseCloud.CallFunctionAsync<string>("BestwayDelete", iDic);
                ParseInstance = "iVanDebug";
            }

            await SQLUpdate($"INSERT INTO tblCloudFilesDeleted SELECT '{ParseInstance}','{FileName}','{xClass}','{Field}','{objectId}',GETDATE(),NULL");
        }
        private async Task CreateXML(string route, string company)
        {
            await CreateXMLFromStoredProc(route, company + "_CSRoT_defaultOffers", "iVan_MakeDefaultOffers_CSRoT", false);
            await CreateXMLFromStoredProc(route, company + "_defaultExcludedCustomerOffers", "iVan_MakeDefaultExcludedCustomerOffers", false);
            await CreateXMLFromStoredProc(route, company + "_defaultCustomers.xml", "iVan_MakeDefaultCustomers", false);
            await CreateXMLFromStoredProc(route, company + "_defaultBusinessSettings.xml", "iVan_defaultBusinessSettings", false);
            await CreateXMLFromStoredProc(route, company + "_defaultAllowedItems.xml", "iVan_MakeDefaultAllowedItems", false);
            await CreateXMLFromStoredProc(route, company + "_defaultCustomerStops.xml", "iVan_MakeDefaultCustomerStops", false);
            await CreateXMLFromStoredProc(route, company + "_defaultItems.xml", "iVan_MakeDefaultItems", false);
            await CreateXMLFromStoredProc(route, company + "_defaultItemSalesTargets.xml", "iVan_MakeDefaultItemSalesTargets", false);
            await CreateXMLFromStoredProc(route, company + "_DefaultPricingData.xml", "iVan_MakeDefaultPricingData", false);
            await CreateXMLFromStoredProc(route, company + "_defaultRouteCluster.xml", "iVan_MakeDefaultRouteCluster", false);
            await CreateXMLFromStoredProc(route, company + "_defaultSpecialPricing.xml", "iVan_MakeDefaultSpecialPricing", false);
            await CreateXMLFromStoredProc(route, company + "_defaultstandardtargets.xml", "iVan_MakeDefaultStandardTargets", false);
            await CreateXMLFromStoredProc(route, company + "_defaultSupplierItems.xml", "iVan_MakeDefaultSupplierItems", false);
            await CreateXMLFromStoredProc(route, company + "_defaultItemConsumption.xml", "iVan_MakeItemConsumption", false);
            await CreateXMLFromStoredProc(route, company + "_defaultInstoreSurveys.xml", "iVan_MakeDefaultInstoreSurveys", false);
            await CreateXMLFromStoredProc(route, company + "_defaultBingo.xml", "iVan_MakeDefaultItemBingo", false);

            await ZipAndUpload(route, company,false);
        }
        private string GetTimeDifferenceInMinutes(DateTime dateFrom, DateTime dateTo)
        {
            var TS = dateTo - dateFrom;
            int hour = TS.Hours;
            int mins = TS.Minutes;
            int secs = TS.Seconds;
            string timeDiff = "";
            if (mins >= 1 & hour == 0)
                timeDiff = mins.ToString("00") + " minutes " + secs.ToString("00") + " seconds";
            else if (mins >= 1 & hour == 1)
                timeDiff = hour.ToString() + " hour " + mins.ToString("00") + " minutes " + secs.ToString("00") + " seconds";
            else if (mins >= 1 & hour > 1)
                timeDiff = hour.ToString() + " hours " + mins.ToString("00") + " minutes " + secs.ToString("00") + " seconds";
            else
                timeDiff = (hour * 60 * 60 + mins * 60 + secs).ToString() + " seconds";
            return timeDiff;
        }

        private void AddOne_Click(object sender, EventArgs e)
        {
            for (int i = 0; i <= ListBox1.SelectedItems.Count-1; i++)
            {
                string csr = Strings.Left(ListBox1.SelectedItems[i].ToString(), 4).Trim();
                ListViewItem tmp = lstViewRoutes.FindItemWithText(csr);
                if (tmp == null)
                {
                    lstViewRoutes.Items.Add(csr);
                }
            }
        }

        private void Button9_Click(object sender, EventArgs e)
        {
            for (int i = 0; i <= ListBox1.Items.Count-1; i++)
            {
                string csr = Strings.Left(ListBox1.Items[i].ToString(), 4).Trim();
                ListViewItem tmp = lstViewRoutes.FindItemWithText(csr);
                if (tmp == null)
                {
                    lstViewRoutes.Items.Add(csr);
                }
            }
        }

        private void Button10_Click(object sender, EventArgs e)
        {
            for (int i = lstViewRoutes.Items.Count-1; i >= 0; i--)
            {
                if (lstViewRoutes.Items[i].Selected)
                {
                    lstViewRoutes.Items.RemoveAt(i);
                }
            }
        }

        private void Button11_Click(object sender, EventArgs e)
        {
            lstViewRoutes.Clear();
        }

        private void Button16_Click(object sender, EventArgs e)
        {

            string jor = JustOneRoute.Text.Replace(" ", "");

            if (jor == "")
            {
                return;
            }

            lstViewRoutes.Clear();

            foreach (string s in jor.Split(','))
            {
                ListViewItem tmp = lstViewRoutes.FindItemWithText(s);
                if (tmp == null)
                {
                    lstViewRoutes.Items.Add(s);
                }
            }

        }

        private async void Button15_Click(object sender, EventArgs e)
        {
            SwitchControls(false);
            ListViewItem l;

            for (int i = lstViewRoutes.Items.Count-1; i >=0; i--)
            {
                l = lstViewRoutes.Items[i];
                if (l.SubItems[0].ForeColor != Color.Red)
                {
                    lstViewRoutes.Items.Remove(l);
                }
            }

            if (lstViewRoutes.Items.Count != 0)
            {
                MessageBox.Show("Press OK to begin data gen for errored routes");
                await CmdGo();
            }
            else { MessageBox.Show("Nothing to do!?!?  Duh"); }

            SwitchControls(true);

        }

        private async void Button6_Click(object sender, EventArgs e)
        {
            if (Business.SelectedItem == null)
            {
                return;
            }

            InitParse();

            SwitchControls(false);

            lstStatus.Items.Clear();
            lstStatus.Items.Insert(0, "Uploading Data ");
            string selItem = Strings.Left(Business.SelectedItem.ToString(), 5).ToLower();

            await GenerateGenericData(Business.SelectedItem.ToString());
            lstStatus.Items.Insert(0, "Uploaded Data ");

            SwitchControls(true);
        }

        private void Business_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Business.SelectedItem != null)
            { 
               UpdateAvailableCSRList(Business.SelectedItem.ToString(), "");
               ASM.SelectedIndex = -1;
            }
        }
        private void UpdateAvailableCSRList(string biz, string asm)
        {
            ListBox1.Items.Clear();

            if (biz == "") {biz = "business";}
            else { biz = $"'{biz}'"; }

            if (asm == "") { asm = "[Area Manager]"; }
            else { asm = $"'{asm}'"; }

            string sql = $"SELECT CAST(r.routeNo AS VARCHAR) + ' - ' + v.CSRName CSR FROM LiveRoutes r LEFT JOIN zeus.dbo.vwAllVSRsInfo v ON r.routeNo = v.RouteNo WHERE liveDate < GETDATE()+25 AND retired=0 AND business = {biz} AND [Area Manager] = {asm} ORDER BY 1";

            SqlConnection SQLCon = new SqlConnection
            {
                ConnectionString = LiveConnString
            };
            SQLCon.Open();
            SqlCommand sqlCmd = new SqlCommand(sql, SQLCon) { CommandTimeout = 6000 };
            SqlDataReader rdr = sqlCmd.ExecuteReader();

            while (rdr.Read())
            {
                ListBox1.Items.Add(rdr["CSR"]);
            }
            rdr.Close();
            SQLCon.Close();

        }

        private void ASM_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ASM.SelectedItem != null)
            {
                UpdateAvailableCSRList("", ASM.SelectedItem.ToString());
                Business.SelectedIndex = -1;
            }
        }
        private void SwitchControls(bool setEnabled)
        {
            foreach (Control c in this.Controls)
            {
                if (c.GetType() != typeof(Label) && c.GetType() != typeof(ListView))
                {
                    c.Enabled = setEnabled;
                }
                //Console.WriteLine($"{c.GetType()},{c.Name}");
            }

        }
        private void InitParse()
        {
            if (SendToLive.Checked)
            {
                sk.InitialiseParse("IvanLive");
                savePath = savePath.Replace("testData", "liveData");
            }
            else
            {
                sk.InitialiseParse("IvanDebug");
                savePath = savePath.Replace("liveData", "testData");
            }
        }



    }
}
