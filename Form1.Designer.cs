﻿namespace DataGen3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.SellingDay = new System.Windows.Forms.DateTimePicker();
            this.NotificationsChk = new System.Windows.Forms.CheckBox();
            this.lstViewRoutes = new System.Windows.Forms.ListView();
            this.ColumnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblProjTime = new System.Windows.Forms.Label();
            this.Button6 = new System.Windows.Forms.Button();
            this.JustOneRoute = new System.Windows.Forms.TextBox();
            this.Label7 = new System.Windows.Forms.Label();
            this.UpdateGenerics = new System.Windows.Forms.CheckBox();
            this.PerformClean = new System.Windows.Forms.CheckBox();
            this.Label6 = new System.Windows.Forms.Label();
            this.Label5 = new System.Windows.Forms.Label();
            this.Button11 = new System.Windows.Forms.Button();
            this.Button10 = new System.Windows.Forms.Button();
            this.Button9 = new System.Windows.Forms.Button();
            this.AddOne = new System.Windows.Forms.Button();
            this.ListBox1 = new System.Windows.Forms.ListBox();
            this.ASM = new System.Windows.Forms.ComboBox();
            this.Business = new System.Windows.Forms.ComboBox();
            this.ShowErrors = new System.Windows.Forms.CheckBox();
            this.Button15 = new System.Windows.Forms.Button();
            this.runningTimeLbl = new System.Windows.Forms.Label();
            this.startTimeLbl = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.lblCount = new System.Windows.Forms.Label();
            this.ProgressBar1 = new System.Windows.Forms.ProgressBar();
            this.SendToLive = new System.Windows.Forms.CheckBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.lstStatus = new System.Windows.Forms.ListBox();
            this.Label8 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.Button16 = new System.Windows.Forms.Button();
            this.cmdGenData = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // SellingDay
            // 
            this.SellingDay.Location = new System.Drawing.Point(197, 146);
            this.SellingDay.Name = "SellingDay";
            this.SellingDay.Size = new System.Drawing.Size(208, 20);
            this.SellingDay.TabIndex = 136;
            // 
            // NotificationsChk
            // 
            this.NotificationsChk.AutoSize = true;
            this.NotificationsChk.BackColor = System.Drawing.Color.Transparent;
            this.NotificationsChk.Checked = true;
            this.NotificationsChk.CheckState = System.Windows.Forms.CheckState.Checked;
            this.NotificationsChk.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NotificationsChk.Location = new System.Drawing.Point(7, 146);
            this.NotificationsChk.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.NotificationsChk.Name = "NotificationsChk";
            this.NotificationsChk.Size = new System.Drawing.Size(176, 23);
            this.NotificationsChk.TabIndex = 135;
            this.NotificationsChk.Text = "Send Notifications";
            this.NotificationsChk.UseVisualStyleBackColor = false;
            // 
            // lstViewRoutes
            // 
            this.lstViewRoutes.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.ColumnHeader1});
            this.lstViewRoutes.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstViewRoutes.HideSelection = false;
            this.lstViewRoutes.Location = new System.Drawing.Point(454, 91);
            this.lstViewRoutes.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lstViewRoutes.Name = "lstViewRoutes";
            this.lstViewRoutes.Scrollable = false;
            this.lstViewRoutes.Size = new System.Drawing.Size(524, 407);
            this.lstViewRoutes.TabIndex = 116;
            this.lstViewRoutes.UseCompatibleStateImageBehavior = false;
            this.lstViewRoutes.View = System.Windows.Forms.View.List;
            // 
            // lblProjTime
            // 
            this.lblProjTime.BackColor = System.Drawing.Color.Transparent;
            this.lblProjTime.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProjTime.ForeColor = System.Drawing.Color.Black;
            this.lblProjTime.Location = new System.Drawing.Point(671, 17);
            this.lblProjTime.Name = "lblProjTime";
            this.lblProjTime.Size = new System.Drawing.Size(307, 30);
            this.lblProjTime.TabIndex = 134;
            this.lblProjTime.Text = "Projected Remaining Time :";
            this.lblProjTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Button6
            // 
            this.Button6.BackColor = System.Drawing.Color.Transparent;
            this.Button6.BackgroundImage = global::DataGen3.Properties.Resources.gsgd;
            this.Button6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Button6.FlatAppearance.BorderSize = 0;
            this.Button6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Button6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button6.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button6.Location = new System.Drawing.Point(12, 286);
            this.Button6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Button6.Name = "Button6";
            this.Button6.Size = new System.Drawing.Size(145, 78);
            this.Button6.TabIndex = 111;
            this.Button6.UseVisualStyleBackColor = false;
            this.Button6.Click += new System.EventHandler(this.Button6_Click);
            // 
            // JustOneRoute
            // 
            this.JustOneRoute.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.JustOneRoute.Location = new System.Drawing.Point(197, 361);
            this.JustOneRoute.MaxLength = 1000;
            this.JustOneRoute.Name = "JustOneRoute";
            this.JustOneRoute.Size = new System.Drawing.Size(154, 20);
            this.JustOneRoute.TabIndex = 132;
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.BackColor = System.Drawing.Color.Transparent;
            this.Label7.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label7.Location = new System.Drawing.Point(193, 390);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(61, 19);
            this.Label7.TabIndex = 131;
            this.Label7.Text = "Status";
            // 
            // UpdateGenerics
            // 
            this.UpdateGenerics.AutoSize = true;
            this.UpdateGenerics.BackColor = System.Drawing.Color.Transparent;
            this.UpdateGenerics.Checked = true;
            this.UpdateGenerics.CheckState = System.Windows.Forms.CheckState.Checked;
            this.UpdateGenerics.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UpdateGenerics.Location = new System.Drawing.Point(7, 116);
            this.UpdateGenerics.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UpdateGenerics.Name = "UpdateGenerics";
            this.UpdateGenerics.Size = new System.Drawing.Size(162, 23);
            this.UpdateGenerics.TabIndex = 130;
            this.UpdateGenerics.Text = "Update Generics";
            this.UpdateGenerics.UseVisualStyleBackColor = false;
            // 
            // PerformClean
            // 
            this.PerformClean.AutoSize = true;
            this.PerformClean.BackColor = System.Drawing.Color.Transparent;
            this.PerformClean.Checked = true;
            this.PerformClean.CheckState = System.Windows.Forms.CheckState.Checked;
            this.PerformClean.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PerformClean.Location = new System.Drawing.Point(7, 85);
            this.PerformClean.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.PerformClean.Name = "PerformClean";
            this.PerformClean.Size = new System.Drawing.Size(145, 23);
            this.PerformClean.TabIndex = 129;
            this.PerformClean.Text = "Perform Clean";
            this.PerformClean.UseVisualStyleBackColor = false;
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.BackColor = System.Drawing.Color.Transparent;
            this.Label6.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label6.Location = new System.Drawing.Point(6, 238);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(45, 19);
            this.Label6.TabIndex = 128;
            this.Label6.Text = "ASM";
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.BackColor = System.Drawing.Color.Transparent;
            this.Label5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label5.Location = new System.Drawing.Point(6, 189);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(79, 19);
            this.Label5.TabIndex = 127;
            this.Label5.Text = "Business";
            // 
            // Button11
            // 
            this.Button11.BackColor = System.Drawing.Color.Transparent;
            this.Button11.BackgroundImage = global::DataGen3.Properties.Resources.removeall;
            this.Button11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Button11.FlatAppearance.BorderSize = 0;
            this.Button11.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Button11.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button11.Location = new System.Drawing.Point(364, 310);
            this.Button11.Name = "Button11";
            this.Button11.Size = new System.Drawing.Size(40, 34);
            this.Button11.TabIndex = 126;
            this.Button11.UseVisualStyleBackColor = false;
            this.Button11.Click += new System.EventHandler(this.Button11_Click);
            // 
            // Button10
            // 
            this.Button10.BackColor = System.Drawing.Color.Transparent;
            this.Button10.BackgroundImage = global::DataGen3.Properties.Resources.removeone;
            this.Button10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Button10.FlatAppearance.BorderSize = 0;
            this.Button10.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Button10.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button10.Location = new System.Drawing.Point(364, 270);
            this.Button10.Name = "Button10";
            this.Button10.Size = new System.Drawing.Size(40, 34);
            this.Button10.TabIndex = 125;
            this.Button10.UseVisualStyleBackColor = false;
            this.Button10.Click += new System.EventHandler(this.Button10_Click);
            // 
            // Button9
            // 
            this.Button9.BackColor = System.Drawing.Color.Transparent;
            this.Button9.BackgroundImage = global::DataGen3.Properties.Resources.addall;
            this.Button9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Button9.FlatAppearance.BorderSize = 0;
            this.Button9.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Button9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button9.Location = new System.Drawing.Point(364, 229);
            this.Button9.Name = "Button9";
            this.Button9.Size = new System.Drawing.Size(40, 34);
            this.Button9.TabIndex = 124;
            this.Button9.UseVisualStyleBackColor = false;
            this.Button9.Click += new System.EventHandler(this.Button9_Click);
            // 
            // AddOne
            // 
            this.AddOne.BackColor = System.Drawing.Color.Transparent;
            this.AddOne.BackgroundImage = global::DataGen3.Properties.Resources.addone;
            this.AddOne.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.AddOne.FlatAppearance.BorderSize = 0;
            this.AddOne.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.AddOne.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.AddOne.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddOne.Location = new System.Drawing.Point(364, 189);
            this.AddOne.Name = "AddOne";
            this.AddOne.Size = new System.Drawing.Size(40, 34);
            this.AddOne.TabIndex = 123;
            this.AddOne.UseVisualStyleBackColor = false;
            this.AddOne.Click += new System.EventHandler(this.AddOne_Click);
            // 
            // ListBox1
            // 
            this.ListBox1.FormattingEnabled = true;
            this.ListBox1.Location = new System.Drawing.Point(197, 189);
            this.ListBox1.Name = "ListBox1";
            this.ListBox1.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.ListBox1.Size = new System.Drawing.Size(154, 160);
            this.ListBox1.TabIndex = 122;
            // 
            // ASM
            // 
            this.ASM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ASM.FormattingEnabled = true;
            this.ASM.Location = new System.Drawing.Point(6, 260);
            this.ASM.Name = "ASM";
            this.ASM.Size = new System.Drawing.Size(165, 21);
            this.ASM.TabIndex = 121;
            this.ASM.SelectedIndexChanged += new System.EventHandler(this.ASM_SelectedIndexChanged);
            // 
            // Business
            // 
            this.Business.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Business.FormattingEnabled = true;
            this.Business.Location = new System.Drawing.Point(6, 211);
            this.Business.Name = "Business";
            this.Business.Size = new System.Drawing.Size(165, 21);
            this.Business.TabIndex = 120;
            this.Business.SelectedIndexChanged += new System.EventHandler(this.Business_SelectedIndexChanged);
            // 
            // ShowErrors
            // 
            this.ShowErrors.AutoSize = true;
            this.ShowErrors.BackColor = System.Drawing.Color.Transparent;
            this.ShowErrors.Checked = true;
            this.ShowErrors.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ShowErrors.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ShowErrors.Location = new System.Drawing.Point(7, 54);
            this.ShowErrors.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ShowErrors.Name = "ShowErrors";
            this.ShowErrors.Size = new System.Drawing.Size(126, 23);
            this.ShowErrors.TabIndex = 119;
            this.ShowErrors.Text = "Show Errors";
            this.ShowErrors.UseVisualStyleBackColor = false;
            // 
            // Button15
            // 
            this.Button15.BackColor = System.Drawing.Color.Transparent;
            this.Button15.BackgroundImage = global::DataGen3.Properties.Resources.rfr;
            this.Button15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Button15.FlatAppearance.BorderSize = 0;
            this.Button15.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Button15.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Button15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button15.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button15.Location = new System.Drawing.Point(12, 361);
            this.Button15.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Button15.Name = "Button15";
            this.Button15.Size = new System.Drawing.Size(147, 133);
            this.Button15.TabIndex = 118;
            this.Button15.UseVisualStyleBackColor = false;
            this.Button15.Click += new System.EventHandler(this.Button15_Click);
            // 
            // runningTimeLbl
            // 
            this.runningTimeLbl.AutoSize = true;
            this.runningTimeLbl.BackColor = System.Drawing.Color.Transparent;
            this.runningTimeLbl.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.runningTimeLbl.Location = new System.Drawing.Point(644, 67);
            this.runningTimeLbl.Name = "runningTimeLbl";
            this.runningTimeLbl.Size = new System.Drawing.Size(20, 16);
            this.runningTimeLbl.TabIndex = 115;
            this.runningTimeLbl.Text = "   ";
            // 
            // startTimeLbl
            // 
            this.startTimeLbl.AutoSize = true;
            this.startTimeLbl.BackColor = System.Drawing.Color.Transparent;
            this.startTimeLbl.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startTimeLbl.Location = new System.Drawing.Point(644, 51);
            this.startTimeLbl.Name = "startTimeLbl";
            this.startTimeLbl.Size = new System.Drawing.Size(20, 16);
            this.startTimeLbl.TabIndex = 114;
            this.startTimeLbl.Text = "   ";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.BackColor = System.Drawing.Color.Transparent;
            this.Label4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label4.Location = new System.Drawing.Point(552, 67);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(96, 16);
            this.Label4.TabIndex = 113;
            this.Label4.Text = "Running Time :";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.BackColor = System.Drawing.Color.Transparent;
            this.Label3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.Location = new System.Drawing.Point(570, 51);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(78, 16);
            this.Label3.TabIndex = 112;
            this.Label3.Text = "Start Time :";
            // 
            // lblCount
            // 
            this.lblCount.AutoSize = true;
            this.lblCount.BackColor = System.Drawing.Color.Transparent;
            this.lblCount.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCount.Location = new System.Drawing.Point(451, 67);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(27, 16);
            this.lblCount.TabIndex = 110;
            this.lblCount.Text = "0/0";
            // 
            // ProgressBar1
            // 
            this.ProgressBar1.Location = new System.Drawing.Point(788, 55);
            this.ProgressBar1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ProgressBar1.Name = "ProgressBar1";
            this.ProgressBar1.Size = new System.Drawing.Size(190, 28);
            this.ProgressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.ProgressBar1.TabIndex = 109;
            // 
            // SendToLive
            // 
            this.SendToLive.AutoSize = true;
            this.SendToLive.BackColor = System.Drawing.Color.Transparent;
            this.SendToLive.Checked = true;
            this.SendToLive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.SendToLive.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SendToLive.Location = new System.Drawing.Point(7, 17);
            this.SendToLive.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.SendToLive.Name = "SendToLive";
            this.SendToLive.Size = new System.Drawing.Size(158, 29);
            this.SendToLive.TabIndex = 108;
            this.SendToLive.Text = "Send to Live";
            this.SendToLive.UseVisualStyleBackColor = false;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.BackColor = System.Drawing.Color.Transparent;
            this.Label2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.Location = new System.Drawing.Point(451, 51);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(44, 16);
            this.Label2.TabIndex = 107;
            this.Label2.Text = "Status";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.BackColor = System.Drawing.Color.Transparent;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(450, 15);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(187, 24);
            this.Label1.TabIndex = 105;
            this.Label1.Text = "Routes To Process";
            // 
            // lstStatus
            // 
            this.lstStatus.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstStatus.FormattingEnabled = true;
            this.lstStatus.ItemHeight = 16;
            this.lstStatus.Location = new System.Drawing.Point(197, 413);
            this.lstStatus.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lstStatus.Name = "lstStatus";
            this.lstStatus.Size = new System.Drawing.Size(208, 84);
            this.lstStatus.TabIndex = 106;
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.BackColor = System.Drawing.Color.Transparent;
            this.Label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label8.Location = new System.Drawing.Point(195, 128);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(92, 18);
            this.Label8.TabIndex = 137;
            this.Label8.Text = "Selling Day";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Button16
            // 
            this.Button16.BackColor = System.Drawing.Color.Transparent;
            this.Button16.BackgroundImage = global::DataGen3.Properties.Resources.greenadd;
            this.Button16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Button16.FlatAppearance.BorderSize = 0;
            this.Button16.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Button16.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Button16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button16.Location = new System.Drawing.Point(364, 352);
            this.Button16.Name = "Button16";
            this.Button16.Size = new System.Drawing.Size(40, 34);
            this.Button16.TabIndex = 133;
            this.Button16.UseVisualStyleBackColor = false;
            this.Button16.Click += new System.EventHandler(this.Button16_Click);
            // 
            // cmdGenData
            // 
            this.cmdGenData.AutoSize = true;
            this.cmdGenData.BackColor = System.Drawing.Color.Transparent;
            this.cmdGenData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.cmdGenData.FlatAppearance.BorderSize = 0;
            this.cmdGenData.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.cmdGenData.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.cmdGenData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdGenData.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdGenData.Image = global::DataGen3.Properties.Resources.GD2;
            this.cmdGenData.Location = new System.Drawing.Point(198, 8);
            this.cmdGenData.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmdGenData.Name = "cmdGenData";
            this.cmdGenData.Size = new System.Drawing.Size(215, 116);
            this.cmdGenData.TabIndex = 117;
            this.cmdGenData.UseVisualStyleBackColor = false;
            this.cmdGenData.Click += new System.EventHandler(this.cmdGenData_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(990, 507);
            this.Controls.Add(this.SellingDay);
            this.Controls.Add(this.NotificationsChk);
            this.Controls.Add(this.lstViewRoutes);
            this.Controls.Add(this.lblProjTime);
            this.Controls.Add(this.Button6);
            this.Controls.Add(this.Button16);
            this.Controls.Add(this.JustOneRoute);
            this.Controls.Add(this.Label7);
            this.Controls.Add(this.UpdateGenerics);
            this.Controls.Add(this.PerformClean);
            this.Controls.Add(this.Label6);
            this.Controls.Add(this.Label5);
            this.Controls.Add(this.Button11);
            this.Controls.Add(this.Button10);
            this.Controls.Add(this.Button9);
            this.Controls.Add(this.AddOne);
            this.Controls.Add(this.ListBox1);
            this.Controls.Add(this.ASM);
            this.Controls.Add(this.Business);
            this.Controls.Add(this.ShowErrors);
            this.Controls.Add(this.Button15);
            this.Controls.Add(this.cmdGenData);
            this.Controls.Add(this.runningTimeLbl);
            this.Controls.Add(this.startTimeLbl);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.lblCount);
            this.Controls.Add(this.ProgressBar1);
            this.Controls.Add(this.SendToLive);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.lstStatus);
            this.Controls.Add(this.Label8);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.DateTimePicker SellingDay;
        internal System.Windows.Forms.CheckBox NotificationsChk;
        internal System.Windows.Forms.ListView lstViewRoutes;
        internal System.Windows.Forms.ColumnHeader ColumnHeader1;
        internal System.Windows.Forms.Label lblProjTime;
        internal System.Windows.Forms.Button Button6;
        internal System.Windows.Forms.Button Button16;
        internal System.Windows.Forms.TextBox JustOneRoute;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.CheckBox UpdateGenerics;
        internal System.Windows.Forms.CheckBox PerformClean;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.Button Button11;
        internal System.Windows.Forms.Button Button10;
        internal System.Windows.Forms.Button Button9;
        internal System.Windows.Forms.Button AddOne;
        internal System.Windows.Forms.ListBox ListBox1;
        internal System.Windows.Forms.ComboBox ASM;
        internal System.Windows.Forms.ComboBox Business;
        internal System.Windows.Forms.CheckBox ShowErrors;
        internal System.Windows.Forms.Button Button15;
        internal System.Windows.Forms.Button cmdGenData;
        internal System.Windows.Forms.Label runningTimeLbl;
        internal System.Windows.Forms.Label startTimeLbl;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label lblCount;
        internal System.Windows.Forms.ProgressBar ProgressBar1;
        internal System.Windows.Forms.CheckBox SendToLive;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.ListBox lstStatus;
        internal System.Windows.Forms.Label Label8;
        private System.Windows.Forms.Timer timer1;
    }
}

